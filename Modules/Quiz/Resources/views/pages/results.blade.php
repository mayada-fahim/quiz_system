@extends('quiz::layouts.userMaster')

@section('content')
<div class="container margin-top">
    <h2>Your Result</h2>

    <div class="row">
        <div class="col">
            <h2> Your score is {{$result["result_points"]}}</h2>
            <p>{{$result["result_details"]}}</p>
        </div>
    </div>
</div>
@stop

<!--Required js files-->
@section('pagescript')
@stop
