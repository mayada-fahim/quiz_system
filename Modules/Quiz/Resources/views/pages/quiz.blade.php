@extends('quiz::layouts.userMaster')

@section('content')
<div class="container margin-top">
    @if($flag == "true")
    <div class="row">
        <h2>{{$quiz["name"]}}</h2>

        <!--quiz count down-->
        <div class="countdown pull-right">
            <h2> Remaining time
                <span id="clock"></span>
                <input type="hidden"  id="time" value="{{$quiz["time"]}}"/></h2>
        </div>
    </div>
    <div class="row">

        {{ Form::model([], ['route' => 'submit-quiz', "id" => "submit"]) }}
        {{Form::hidden("quiz_id",$quiz["id"])}}
        @foreach($quiz["questions"] as $key => $question)
        <div class="row">
            <h4>{{$key+1}} - {{explode('*/*/',$question["question"])[0]}}</h4>
            <span class="code">{{explode('*/*/',$question["question"])[1]}}</span>
            {{Form::hidden("questions[]",$question["points"])}}
            <label>Answers</label>
            @foreach($question["answers"] as $answer)
            <div class="form-group">
                <label>{{ Form::radio('answers['.$key.']', $answer["id"], false) }} {{$answer["answer"]}}</label>
            </div>
            @endforeach
        </div>
        @endforeach
        <div class="form-group">	
            {{ Form::submit('Submit', ['class' => 'btn btn-primary'] )}}
        </div>
        {{Form::close()}}
    </div>
    @else
    <h2>You already took this quiz</h2>
    @endif
</div>
@stop

<!--Required js files-->
@section('pagescript')
<script src="{{ Module::asset('Quiz:js/jquery.countdown.min.js') }}" /></script>
<script src="{{ Module::asset('Quiz:js/quiz.js') }}" /></script>
@stop
