@extends('quiz::layouts.master')

@section('content')
<div class="container margin-top">
    <h2>List Quiz Questions</h2>
    <div class="row">

        <div class="col">
            <!--success message-->
            @if(count($questions) == 0)

            <p class="alert alert-warning"> This quiz has no questions yet ,click on add new question </p>
            @endif

            {{ Form::open(['method' => 'DELETE', 'route' => 'delete-question-from-quiz']) }}
            <table class="table table-striped table-bordered table-hover row">

                <!--Table head-->
                <thead>
                    <tr>
                        <th width="2%"></th>
                        <th>Question</th>
                        <th>Category</th>
                        <th>Type</th>
                        <th>Points</th>
                        <th>Correct Answer</th>
                        <th>Edit</th>
                    </tr>
                </thead>

                <!--Table Body-->
                <tbody>
                    @foreach ($questions as $question)
                    <tr>
                        <td><input type="checkbox" name="ids[]" value="{{$question->id}}" class="select"/></td>
                        <td>{{explode('*/*/',$question["question"])[0]}}
                            <span class="code">{{explode('*/*/',$question["question"])[1]}}</span></td>
                        <td>{{$question["category"]["name"]}}</td>
                        <td>{{$question["questionType"]["type"]}}</td>
                        <td>{{$question["points"]}}</td>
                        <td>{{$question["answers"][0]["answer"]}}</td>
                        <td><a href="{{url('question/edit/'.$question->id)}}" class="btn btn-warning">Edit</a></td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
 
            <div class="row pull-right">
                {{ Form::hidden('quiz_id', $id) }}
                {{ Form::submit('Delete selected', ['class' => 'btn btn-danger actionBtn', 'disabled']) }}
                <a class="btn btn-primary" href="{{ url('quiz/questions/create/'.$id)}}"> Add question to this quiz</a>
            </div>
            {{ Form::close() }}
        </div>
    </div>
</div>
@stop

<!--Required js files-->
@section('pagescript')
<script src="{{ Module::asset('Quiz:js/list.js') }}" /></script>
@stop
