@extends('quiz::layouts.master')

@section('content')
<div class="container margin-top">
    <h2>Users who took this quiz</h2>
    <form action="{{ url("quiz/".$id."/users") }}" method="POST" class="delete row pull-right">
        {{ csrf_field() }}
        {{ method_field('DELETE') }}

        <button class="btn btn-danger" style="margin-bottom: 20px;">Delete All</button>
    </form>
    <div class="row">
        <div class="col">

            <table class="table table-striped table-bordered table-hover row">

                <!--Table head-->
                <thead>
                    <tr>
                        <th>Rank</th>
                        <th>Name</th>
                        <th>Email</th>
                    </tr>
                </thead>

                <!--Table Body-->
                <tbody>
                    @foreach ($users as $key=>$user)
                    <tr>
                        <td>{{$key +1}}</td>
                        <td>{{$user["username"]}}</td>
                        <td>{{$user["email"]}}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@stop

<!--Required js files-->
@section('pagescript')
@stop
