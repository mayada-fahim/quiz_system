@extends('quiz::layouts.master')

@section('content')
<div class="container margin-top">
    <h2>List Quizzes</h2>

    <div class="row">
        <div class="col">
            <!--success message-->
            @if(Session::has('alert-success'))

            <p class="alert alert-success">{{ Session::get('alert-success') }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
            @endif

            <table class="table table-striped table-bordered table-hover row">

                <!--Table head-->
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Category</th>
                        <th>Duration(min)</th>
                        <th>Total Points</th>
                        <th>Pass Points</th>
                        <th>Edit</th>
                        <th>Delete</th>
                        <th>Questions</th>
                        <th>Results</th>
                    </tr>
                </thead>

                <!--Table Body-->
                <tbody>
                    @foreach ($quizzes as $quiz)
                    <tr>
                        <td>{{$quiz["name"]}}</td>
                        <td>{{$quiz["category"]["name"]}}</td>
                        <td>{{$quiz["time"]}}</td>
                        <td>{{$quiz["total_points"]}}</td>
                        <td>{{$quiz["pass_points"]}}</td>
                        <td>
                            <a href="{{ url('quiz/edit/'.$quiz["id"]) }}"class="btn btn-warning">Edit</a>
                        </td>
                        <td>
                            <form action="quiz/{{ $quiz->id }}" method="POST" class="delete" >
                                {{ csrf_field() }}
                                {{ method_field('DELETE') }}

                                <button class="btn btn-danger">Delete</button>
                            </form>
                        </td>
                        <td>
                            <a href="{{ url('quiz/questions/'.$quiz["id"])}}" class="btn btn-primary"> Manage Questions</a>
                        </td>
                        <td>
                            <div class="dropdown">
                                <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Results
                                    <span class="caret"></span></button>
                                <ul class="dropdown-menu">
                                    <li class="menu-item dropdown"><a href="{{ url('quiz/'.$quiz["id"]."/ranks/top") }}">Top 20</a></li>
                                    <li class="menu-item dropdown"><a href="{{ url('quiz/'.$quiz["id"]."/ranks/all") }}">All</a></li>
                                </ul>
                            </div>

                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>

            <!--Pagination links-->
            <div class="col-xs-12 text-center">
                <nav aria-label="...">
                    {{ $quizzes->links()}}
                </nav>
            </div> 
        </div>
    </div>
</div>
@stop

<!--Required js files-->
@section('pagescript')

<script src="{{ Module::asset('Quiz:js/list.js') }}" /></script>
@stop
