@extends('quiz::layouts.master')

@section('content')
<div class="container margin-top">
    <h2>Add Quiz Questions</h2>

    <div class="row">

        <div class="col">

            <p class="alert alert-warning"> Questions displayed are the ones under the same quiz category only </p>
            {{ Form::model([], ['route' => 'add-questions-to-quiz']) }}
            <table class="table table-striped table-bordered table-hover row">

                <!--Table head-->
                <thead>
                    <tr>
                        <th width="2%"></th>
                        <th>Question</th>
                        <th>Category</th>
                        <th>Type</th>
                        <th>Points</th>
                        <th>Correct Answer</th>
                    </tr>
                </thead>

                <!--Table Body-->
                <tbody>
                    @foreach ($questions as $question)
                    <tr>
                        <td><input type="checkbox" name="ids[]" value="{{$question->id}}" class="select"/></td>
                        <td>{{explode('*/*/',$question["question"])[0]}}
                            <span class="code">{{explode('*/*/',$question["question"])[1]}}</span></td>
                        <td>{{$question["category"]["name"]}}</td>
                        <td>{{$question["questionType"]["type"]}}</td>
                        <td>{{$question["points"]}}</td>
                        <td>{{$question["answers"][0]["answer"]}}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>

            <!--Pagination links-->
            <div class="col-xs-12 text-center">
                <nav aria-label="...">
                    {{ $questions->links()}}
                </nav>
            </div>

            {{Form::hidden('quiz_id',$id)}}
            <div class="form-group">	
                {{ Form::submit('Add to quiz', ['class' => 'btn btn-primary pull-right actionBtn', 'disabled'] )}}
            </div>

            {{ Form::close() }}
        </div>
    </div>
</div>
@stop

<!--Required js files-->
@section('pagescript')
<script src="{{ Module::asset('Quiz:js/list.js') }}" /></script>
@stop
