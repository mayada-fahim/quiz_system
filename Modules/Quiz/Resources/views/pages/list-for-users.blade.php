@extends('quiz::layouts.userMaster')

@section('content')
<div class="container margin-top">
    <h2>Choose quiz you want to take</h2>

    <div class="row">
        <div class="col">

            <table class="table table-striped table-bordered table-hover row">

                <!--Table head-->
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Category</th>
                        <th>Duration(min)</th>
                        <th>Total Points</th>
                        <th>Pass Points</th>
                        <th></th>
                    </tr>
                </thead>

                <!--Table Body-->
                <tbody>
                    @foreach ($quizzes as $quiz)
                    <tr>
                        <td>{{$quiz["name"]}}</td>
                        <td>{{$quiz["category"]["name"]}}</td>
                        <td>{{$quiz["time"]}}</td>
                        <td>{{$quiz["total_points"]}}</td>
                        <td>{{$quiz["pass_points"]}}</td>
                        <td>
                            <a href="{{ url('quiz/'.$quiz["id"])}}" class="btn btn-warning"> Take this quiz</a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>

            <!--Pagination links-->
            <div class="col-xs-12 text-center">
                <nav aria-label="...">
                    {{ $quizzes->links()}}
                </nav>
            </div> 
        </div>
    </div>
</div>
@stop

<!--Required js files-->
@section('pagescript')
@stop
