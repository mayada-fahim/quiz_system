@extends('quiz::layouts.master')

@section('content')
<div class="container margin-top">
    <h2>Manage Quiz</h2>

    <!-- Form for adding a new Quiz-->
    @if(!isset($quiz["id"]))
    {{ Form::model($quiz, ['route' => 'store-quiz']) }}
    @else
    {{ Form::model($quiz, ['route' => ['update-quiz', $quiz->id], 'method' => 'put']) }}
    @endif

    <!--Quiz category-->
    <div class="form-group">
        {{Form::label('category', 'Please choose quiz category', ['class' => 'control-label', "required"]) }}
        {{Form::select('category_id', $categories,null, ["class" => "form-control"]) }}
    </div>

    <!--Quiz Name-->
    <div class="form-group">
        {{Form::label('name', 'Enter quiz name', ['class' => 'control-label']) }}
        {{Form::text('name',isset($quiz["name"])? $quiz["name"] : "", ["class" => "form-control", "required"]) }}
    </div>

    <!--Quiz Duration-->
    <div class="form-group">
        {{Form::label('time', 'Enter quiz duration in minutes', ['class' => 'control-label']) }}
        {{Form::number('time',isset($quiz["time"])? $quiz["time"] : "", ["class" => "form-control", "min" => 0, "required"]) }}
    </div>

    <!--points-->
    <div class="form-group">
        {{Form::label('total_points', 'Enter quiz total points', ['class' => 'control-label']) }}
        {{Form::number('total_points',isset($quiz["total_points"])? $quiz["total_points"] : "", ["class" => "form-control", "min" => 0, "required"]) }}
    </div>

    <div class="form-group">
        {{Form::label('pass_points', 'Enter quiz pass points', ['class' => 'control-label']) }}
        {{Form::number('pass_points',isset($quiz["pass_points"])? $quiz["pass_points"] : "", ["class" => "form-control", "min" => 0, "required"]) }}
    </div>

    <div class="form-group">	
        {{ Form::submit('Save', ['class' => 'btn btn-primary'] )}}
    </div>

    {{ Form::close() }}

    <!--Error message-->
    @if(Session::has('errors'))
    <div class="alert alert-danger">
        <ul>
            @foreach (Session::get('errors') as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
</div>
@stop

<!--Required js files-->
@section('pagescript')

<!--<script src="{{ Module::asset('Question:js/create.js') }}" /></script>-->
@stop
