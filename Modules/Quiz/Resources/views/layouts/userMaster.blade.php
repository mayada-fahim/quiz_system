<!doctype html>
<html>
    <head>
        @include('includes.head')
    </head>
    <body class="drawer drawer--left">
        <div>

            <header>
                @include('includes.user-header')

            </header>

            <div id="main" role='main'>

                @yield('content')

            </div>
            @include('includes.scripts')
            @yield('pagescript')

    </div>
</body>
</html>