// Confirmation before delete
$(".delete").on("submit", function () {
    return confirm("Do you want to delete this items?");
});

// fill the table with code snippet if found
$('.code').each(function () {

    var $this = $(this),
            $code = $this.html();

    $this.empty();

    if ($code) {
        var myCodeMirror = CodeMirror(this, {
            value: $code,
            mode: 'text/x-php',
            lineNumbers: !$this.is('.inline'),
            readOnly: true
        });
    }

});

$(".select").on("click", function () {
    if ($(".select:checked").length >= 1)
    {
        $('.actionBtn').prop('disabled', false);
    } else
    {
        $('.actionBtn').prop('disabled', true);
    }
});