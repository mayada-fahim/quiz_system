<?php

Route::group(['middleware' => 'web', 'prefix' => 'quiz', 'namespace' => 'Modules\Quiz\Http\Controllers'], function()
{
    Route::get('/', 'QuizController@index');
    Route::get('create', 'QuizController@create');
    Route::post('store', ['as' => 'store-quiz', 'uses' => 'QuizController@store']);
    Route::delete('{id}', 'QuizController@destroy');
    Route::get("edit/{id}", 'QuizController@show');
    Route::put("update/{id}", ['as' => 'update-quiz', 'uses' => 'QuizController@update']);
    Route::get("questions/{id}",'QuizController@getQuestions');
    Route::get("questions/create/{id}", 'QuizController@createQuizQuestions');
    Route::post("questions/add", ["as" => 'add-questions-to-quiz', 'uses' => 'QuizController@addQuestionsToQuiz']);
    Route::delete("questions/delete",['as' => 'delete-question-from-quiz', 'uses' =>'QuizController@deleteQuestionsFromQuiz']);
    Route::get('{id}/ranks/{key}', 'QuizController@getRanks');
    Route::delete('{id}/users', 'QuizController@deleteUsers');
    
    // user routes
    Route::get("all",'QuizController@listQuizzesForUsers');
    Route::get("{id}",'QuizController@getQuiz');
    Route::post("quizResult", ['as' => 'submit-quiz', 'uses' => 'QuizController@submitQuiz']);
});
