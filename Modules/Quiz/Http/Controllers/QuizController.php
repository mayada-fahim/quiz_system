<?php

namespace Modules\Quiz\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Quiz\Services\CategoryService;
use Modules\Quiz\Services\QuizService;
use Illuminate\Support\Facades\Redirect;
use Modules\Question\Services\QuestionService;
use Auth;

class QuizController extends Controller {

    public function __construct(CategoryService $categoryService, QuizService $quizService, QuestionService $questionService) {
        $this->middleware('auth');
        $this->categoryService = $categoryService;
        $this->quizService = $quizService;
        $this->questionService = $questionService;
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index() {
        $quizzes = $this->quizService->getPaginatedList();
        return view('quiz::pages.list', ["quizzes" => $quizzes]);
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create() {
        $categories = $this->categoryService->getAllForDropDown();
        return view('quiz::pages.form', ["quiz" => [], "categories" => $categories]);
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request) {
        $data = $request->all();
        $ret = $this->quizService->create($data);
        if (isset($ret["errors"])) {
            $request->session()->flash('errors', $ret["errors"]);
            return Redirect::to('/quiz/create');
        } else {
            $request->session()->flash('alert-success', 'Quiz has been added successfully');
            return Redirect::to('/quiz');
        }
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show($id) {
        $categories = $this->categoryService->getAllForDropDown();
        $quiz = $this->quizService->getById($id);
        return view('quiz::pages.form', ["quiz" => $quiz, "categories" => $categories]);
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update($id, Request $request) {
        $data = $request->all();
        $ret = $this->quizService->update($id, $data);
        if (isset($ret["errors"])) {
            $request->session()->flash('errors', $ret["errors"]);
            return Redirect::to('/quiz/edit/' . $id);
        } else {
            $request->session()->flash('alert-success', 'Quiz has been added successfully');
            return Redirect::to('/quiz');
        }
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy($id, Request $request) {
        $quiz = $this->quizService->getById($id);
        if ($quiz) {
            $this->quizService->delete($id);
        }
        $request->session()->flash('alert-success', 'Quiz has been deleted successfully');
        return Redirect::to('/quiz');
    }

    /*
     * get questions under certain quiz
     */

    public function getQuestions($id) {
        $questions = $this->quizService->getQuestions($id)["questions"];
        return view('quiz::pages.quiz-questions', ["questions" => $questions, "id" => $id]);
    }

    /*
     * list questions to be chosen in a quiz
     */

    public function createQuizQuestions($id) {
        $quiz = $this->quizService->getById($id);
        $questions = $this->questionService->getByCategory($quiz["category_id"], $id);
        return view('quiz::pages.create-quiz-questions', ["questions" => $questions, "id" => $id]);
    }

    /*
     * add certain question to a quiz
     */

    public function addQuestionsToQuiz(Request $request) {
        $data = $request->all();
        $this->quizService->addQuestionsToQuiz($data);
        return Redirect::to('/quiz/questions/' . $data["quiz_id"]);
    }

    /*
     * delete questions from quiz
     */

    public function deleteQuestionsFromQuiz(Request $request) {
        $data = $request->all();
        $this->quizService->deleteQuestionsFromQuiz($data);
        return Redirect::to('/quiz/questions/' . $data["quiz_id"]);
    }

    /*
     * list quizzes for users
     */

    public function listQuizzesForUsers() {
        $quizzes = $this->quizService->getPaginatedList();
        return view('quiz::pages.list-for-users', ["quizzes" => $quizzes]);
    }

    /*
     * get quiz with questions for user to take
     */

    public function getQuiz($id) {
        $userId = Auth::user()->id;
        $ret = $this->quizService->userQuizCheck($id, $userId);
        if ($ret) {
            $flag = false;
        } else {
            $flag = true;
        }
        $quiz = $this->quizService->getQuiz($id);
        return view('quiz::pages.quiz', ["quiz" => $quiz, "flag" => $flag]);
    }
    
    /*
     * use quiz submission
     */
    public function submitQuiz(Request $request) {
        $data = $request->all();
        $user = Auth::user();
        $data = $this->questionService->calculatePoints($data);
        $ret = $this->quizService->submitQuiz($data, $user->id);
        return view('quiz::pages.results', ["result" => $ret]);
    }

    /*
     * get users who took the quiz with ranks
     */

    public function getRanks($id, $key) {
        $users = $this->quizService->getRanks($id, $key);
        return view('quiz::pages.list-quiz-ranks', ["users" => $users, "id" =>$id]);
    }

    /*
     * delete users who took that quiz
     */

    public function deleteUsers($id, Request $request) {
        $this->quizService->deleteUsers($id);
        $request->session()->flash('alert-success', 'users have been deleted successfully');
        return Redirect::to('/quiz');
    }

}
