<?php

namespace Modules\Quiz\Models;

use Illuminate\Database\Eloquent\Model;

class QuizQuestion extends Model {
    /*
     * Model Parameters
     */

    protected $table = 'quiz_questions';
    protected $fillable = array('question_id', 'quiz_id');

    /*
     * Relations
     */

    public function quizes() {
        return $this->belongsTo('Modules\Quiz\Models\Quiz');
    }

}
