<?php

namespace Modules\Quiz\Models;

use Illuminate\Database\Eloquent\Model;

class UserQuiz extends Model {
    /*
     * Model Parameters
     */

    protected $table = 'user_quizes';
    protected $fillable = array('user_id', 'quiz_id', 'state_id', 'result_details', 'result_points', 'result_state');

    /*
     * Relations
     */

    public function quizes() {
        return $this->belongsTo('Modules\Quiz\Models\Quiz');
    }

}
