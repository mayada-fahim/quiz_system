<?php

namespace Modules\Quiz\Models;

use Illuminate\Database\Eloquent\Model;

class User extends Model {
    
    /*
     * Model Parameters
     */
    protected $table = 'users';
    protected $fillable = array('username', 'email', 'password', 'is_admin');



}
