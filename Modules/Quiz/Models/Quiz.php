<?php

namespace Modules\Quiz\Models;

use Illuminate\Database\Eloquent\Model;

class Quiz extends Model {
    /*
     * Model Parameters
     */

    protected $table = 'quizes';
    protected $fillable = array('name', 'category_id', 'time', 'total_points', 'pass_points');

    /*
     * Validation rules before inserting or updating question
     */
    public static $Rules = [
        'name' => 'required',
        'time' => 'required|integer',
        'category_id' => 'required|integer|exists:lkp_category,id',
        'total_points' => 'required|integer',
        'pass_points' => 'required|integer'
    ];

    /*
     * Relations
     */

    public function category() {
        return $this->belongsTo('Modules\Quiz\Models\Category');
    }

    public function questions() {
        return $this->belongsToMany('Modules\Question\Models\Question', 'quiz_questions');
    }

    public function quizQuestions() {
        return $this->hasMany('Modules\Quiz\Models\QuizQuestion');
    }

    public function users() {
        return $this->belongsToMany('Modules\Quiz\Models\User', 'user_quizes')->orderBy('result_points', 'desc');
    }

    public function userQuizes() {
        return $this->hasMany('Modules\Quiz\Models\UserQuiz');
    }

    // cascade delete quiz questions
    protected static function boot() {
        parent::boot();

        static::deleting(function($quiz) {
            $quiz->quizQuestions()->delete();
        });
    }

}
