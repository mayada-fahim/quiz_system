<?php

namespace Modules\Quiz\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model {
    /*
     * Model Parameters
     */

    protected $table = 'lkp_category';
    protected $fillable = array('name', 'code');

    /*
     * Relations
     */

    public function quizes() {
        return $this->hasMany('Modules\Quiz\Models\Quiz');
    }

}
