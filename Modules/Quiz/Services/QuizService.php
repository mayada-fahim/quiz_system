<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Modules\Quiz\Services;

use Modules\Quiz\Repositories\QuizRepository;
use Modules\Quiz\Repositories\QuizQuestionRepository;
use Modules\Quiz\Repositories\UserQuizRepository;
use Illuminate\Database\DatabaseManager;
use \Illuminate\Database\Eloquent\Model;
use Modules\Quiz\Models\Quiz;
use Validator;

/**
 * Description of CategoryService
 *
 * @author mayada.fahim
 */
class QuizService extends BaseService {

    public function __construct(DatabaseManager $database, QuizRepository $repository, QuizQuestionRepository $quizQuestionRepository, UserQuizRepository $userQuizRepository) {
        $this->setDatabase($database);
        $this->setRepository($repository);
        $this->quizQuestionRepository = $quizQuestionRepository;
        $this->userQuizRepository = $userQuizRepository;
    }

    public function prepareCreate(array $data) {
        // validate the data before insertion
        $rules = Quiz::$Rules;
        $validator = Validator::make($data, $rules);
        if ($validator->fails()) {
            return ["errors" => $validator->errors()->all()];
        }

        $quiz = $this->repository->create($data);
        return $quiz;
    }

    public function prepareUpdate(Model $model, array $data) {
        // validate the data before insertion
        $rules = Quiz::$Rules;
        $validator = Validator::make($data, $rules);
        if ($validator->fails()) {
            return ["errors" => $validator->errors()->all()];
        }
        unset($data["_token"]);
        unset($data["_method"]);
        $this->repository->update($data, $model->id);
    }

    public function prepareDelete($id) {
        $this->repository->delete($id);
    }

    public function getPaginatedList() {
        return $this->repository->getPaginatedList();
    }

    public function getQuestions($id) {
        return $this->repository->getQuestions($id);
    }

    public function addQuestionsToQuiz($data) {
        $quizId = $data["quiz_id"];
        $quiz = $this->getById($quizId);
        foreach ($data["ids"] as $id) {
            $obj = ["question_id" => $id];
            $quiz->quizQuestions()->create($obj);
        }
    }

    public function deleteQuestionsFromQuiz($data) {
        $this->quizQuestionRepository->deleteQuestionsFromQuiz($data);
    }

    public function getQuiz($id) {
        return $this->repository->getQuiz($id);
    }
    
    /*
     * user quiz submission
     */
    public function submitQuiz($data, $userId) {
        $quiz = $this->getById($data["quiz_id"]);
        $obj = [];
        $obj["result_points"] = $data["total_points"];
        $obj["user_id"] = $userId;
        if ($obj["result_points"] >= $quiz["pass_points"]) {
            $obj["result_details"] = "passed the quiz";
        } else {
            $obj["result_details"] = "failed the quiz";
        }
        return $quiz->userQuizes()->create($obj);
    }
    
    /*
     * check if user took the quiz
     */
    public function userQuizCheck($id,$userId)
    {
      return $this->userQuizRepository->userQuizCheck($id,$userId); 
    }
    
    /*
     * get rank holders
     */
    public function getRanks($id,$key)
    {
        return $this->repository->getRanks($id,$key);
    }
    
        /*
     * delete users who took that quiz
     */

    public function deleteUsers($id) {
        return $this->repository->deleteUsers($id);
    }

}
