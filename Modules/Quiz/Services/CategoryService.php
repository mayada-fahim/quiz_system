<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Modules\Quiz\Services;

use Modules\Quiz\Repositories\CategoryRepository;
use Illuminate\Database\DatabaseManager;
use \Illuminate\Database\Eloquent\Model;

/**
 * Description of CategoryService
 *
 * @author mayada.fahim
 */
class CategoryService extends BaseService {

    public function __construct(DatabaseManager $database, CategoryRepository $repository) {
        $this->setDatabase($database);
        $this->setRepository($repository);
    }

    public function prepareCreate(array $data) {

        $category = $this->repository->create($data);
        return $category;
    }

    public function prepareUpdate(Model $model, array $data) {
        $this->repository->update($data, $model->id);
    }

    public function prepareDelete($id) {
        $this->repository->delete($id);
    }
    
    public function getAllForDropDown(){
        $returnedCategories =[];
        $categories = $this->getAll();
        foreach($categories as $category)
        {
            $returnedCategories[$category["id"]] = $category["name"];
        }
        return $returnedCategories;
    }

}
