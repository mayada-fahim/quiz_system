<?php

namespace Modules\Quiz\Repositories;

use Bosnadev\Repositories\Eloquent\Repository;

/**
 * Manage all category Table custom Operations
 *
 * @author mayada.fahim
 */
class UserQuizRepository extends Repository {

    /**
     * Determine the model of the repository
     *
     */
    public function model() {
        return 'Modules\Quiz\Models\UserQuiz';
    }
    
    /*
     * check if user took a specific quiz
     */
    public function userQuizCheck($quizId,$userId) {
        return $this->model
                        ->where("quiz_id", $quizId)
                        ->where('user_id', $userId)->first();
    }


}
