<?php

namespace Modules\Quiz\Repositories;

use Bosnadev\Repositories\Eloquent\Repository;

/**
 * Manage all category Table custom Operations
 *
 * @author mayada.fahim
 */
class QuizRepository extends Repository {

    /**
     * Determine the model of the repository
     *
     */
    public function model() {
        return 'Modules\Quiz\Models\Quiz';
    }
    
    /*
     * get all quizzes in paginated list
     */
    public function getPaginatedList() {
        return $this->model->selectRaw("*")->with(["category"])
                        ->paginate(10);
    }
    
    /*
     * get quiz with its questions
     */
    public function getQuestions($id) {
        return $this->model->where("id", $id)->with(["questions"])->first();
    }
    
    /*
     * get quiz with its questions and answers
     */
    public function getQuiz($id) {
        return $this->model->with(["questions.answers"])->where("id", $id)->first();
    }
    
    /*
     * get rank holders
     */
    public function getRanks($id, $key) {
        $users = $this->model->find($id)->users()->orderBy("result_points");
        if ($key == "all") {
            return $users->get();
        } else {
            return $users->limit(20)->get();
        }
    }

    /*
     * delete users who took that quiz
     */

    public function deleteUsers($id) {
        return $this->model->find($id)->userQuizes()->delete();
    }

}
