<?php

namespace Modules\Quiz\Repositories;

use Bosnadev\Repositories\Eloquent\Repository;

/**
 * Manage all category Table custom Operations
 *
 * @author mayada.fahim
 */
class QuizQuestionRepository extends Repository {

    /**
     * Determine the model of the repository
     *
     */
    public function model() {
        return 'Modules\Quiz\Models\QuizQuestion';
    }
    
    /*
     * remove all quiz questions
     */
    public function deleteQuestionsFromQuiz($data) {
        return $this->model
                        ->where("quiz_id", $data["quiz_id"])
                        ->whereIn('question_id', $data["ids"])->delete();
    }

}
