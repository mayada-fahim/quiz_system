<?php

namespace Modules\Admin\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Admin\Services\UserService;
use Illuminate\Support\Facades\Redirect;
use Auth;
use Illuminate\Support\Facades\Hash;

class AdminController extends Controller {

    public function __construct(UserService $userService) {
        $this->middleware('auth');
        $this->userService = $userService;
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create() {
        return view('admin::pages.create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request) {
        $data = $request->all();
        $ret = $this->userService->create($data);
        if (isset($ret["errors"])) {
            $request->session()->flash('errors', $ret["errors"]);
        } else {
            $request->session()->flash('alert-success', 'Admin has been added successfully');
        }
        return Redirect::to('/admin/create');
    }


    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy() {
        $this->userService->delete(Auth::user()->id);
        return Redirect::to('/');
    }

    /*
     * Reset all tables in database
     */

    public function resetTables() {
        $this->userService->resetTables();
        return Redirect::to('/');
    }

    /*
     * return view for changing admin password
     */

    public function changePassword() {
        return view("admin::pages.change-password");
    }

    /*
     *  update admin password
     */

    public function updatePassword(Request $request) {
        $data = $request->all();
        // check that old password is correct
        if (Hash::check($data["old_password"], Auth::user()->password)) {
            // check that two password match
            if ($data["new_password"] == $data["confirm_password"]) {
                //update admin password
                $this->userService->update(Auth::user()->id, ["password" => bcrypt($data["new_password"])]);
                $request->session()->flash('alert-success', 'Password changed successfully');
            } else {
                // return errors for match passwords
                $request->session()->flash('errors', ['Confirm password doesnot match password']);
            }
        } else {
            //return error for incorrect old password
            $request->session()->flash('errors', ['Old password is incorrect']);
        }
        return Redirect::to('/admin/changePassword');
    }

}
