<?php

Route::group(['middleware' => 'web', 'prefix' => 'admin', 'namespace' => 'Modules\Admin\Http\Controllers'], function() {
    Route::get('create', 'AdminController@create');
    Route::post('store', ['as' => 'store-admin', 'uses' => 'AdminController@store']);
    Route::get('changePassword','AdminController@changePassword');
    Route::put('updatePassword',['as' => 'update-password', 'uses' =>'AdminController@updatePassword']);
    Route::delete('delete', ['as' => 'delete-admin', 'uses' => 'AdminController@destroy']);
    Route::get('reset','AdminController@resetTables');
});
