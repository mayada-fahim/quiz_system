<?php

namespace Modules\Admin\Repositories;

use Bosnadev\Repositories\Eloquent\Repository;
use Illuminate\Support\Facades\Schema;
use DB;

/**
 * Manage all category Table custom Operations
 *
 * @author mayada.fahim
 */
class UserRepository extends Repository {

    /**
     * Determine the model of the repository
     *
     */
    public function model() {
        return 'Modules\Admin\Models\User';
    }

    /*
     * delete all tables from database
     */

    public function resetTables() {
        $tableNames = Schema::getConnection()->getDoctrineSchemaManager()->listTableNames();
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        foreach ($tableNames as $name) {
            DB::table($name)->truncate();
        }
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }

}
