@extends('admin::layouts.master')

@section('content')
<div class="container margin-top">
    <h2>Change Password</h2>
    
    <!--Sucess message-->
    @if(Session::has('alert-success'))

    <p class="alert alert-success">{{ Session::get('alert-success') }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
    @endif

    <!-- Form for updating admin password-->
    {{ Form::model([], ['route' => 'update-password', 'method' => 'put']) }}


    <!--Admin old password-->
    <div class="form-group">
        {{Form::label('old password', 'Enter your old password', ['class' => 'control-label']) }}
        {{Form::password('old_password', ["class" => "form-control", "required"]) }}
    </div>

    <!-- Admin new password-->
    <div class="form-group">
        {{Form::label('new password', 'Enter new password', ['class' => 'control-label']) }}
        {{Form::password('new_password',["class" => "form-control", "required"])}}
    </div>

    <!-- Admin confirmation password-->
    <div class="form-group">

        {{Form::label('password confirm', 'Confirm password', ['class' => 'control-label']) }}
        {{Form::password('confirm_password',["class" => "form-control", "required"])}}
    </div>

    <div class="form-group">	
        {{ Form::submit('Save', ['class' => 'btn btn-primary', 'id' => 'submit'] )}}
    </div>

    {{ Form::close() }}

    <!--Error message-->
    @if(Session::has('errors'))
    <div class="alert alert-danger">
        <ul>
            @foreach (Session::get('errors') as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
</div>

@stop

<!--Required js files-->
@section('pagescript')
@stop