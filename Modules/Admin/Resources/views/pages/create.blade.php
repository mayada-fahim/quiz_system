@extends('admin::layouts.master')

@section('content')
<div class="container margin-top">
    <h2>Add new admin</h2>
    
    <!--success message-->
    @if(Session::has('alert-success'))

    <p class="alert alert-success">{{ Session::get('alert-success') }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
    @endif
    <!-- Form for adding a new admin-->
    {{ Form::model([], ['route' => 'store-admin']) }}


    <!--Admin username-->
    <div class="form-group">
        {{Form::label('username', 'Enter user name', ['class' => 'control-label']) }}
        {{Form::text('username', "", ["class" => "form-control", "required"]) }}
    </div>

    <!-- Admin email-->
    <div class="form-group">
        {{Form::label('email', 'Enter email', ['class' => 'control-label']) }}
        {{Form::email('email','',["class" => "form-control", "required"])}}
    </div>

    <!-- Admin password-->
    <div class="form-group">

        {{Form::label('password', 'Enter password', ['class' => 'control-label']) }}
        {{Form::password('password',["class" => "form-control", "required"])}}
    </div>

    <div class="form-group">	
        {{ Form::submit('Save', ['class' => 'btn btn-primary', 'id' => 'submit'] )}}
    </div>

    {{ Form::close() }}

    <!--Error message-->
    @if(Session::has('errors'))
    <div class="alert alert-danger">
        <ul>
            @foreach (Session::get('errors') as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
</div>

@stop

<!--Required js files-->
@section('pagescript')

@stop