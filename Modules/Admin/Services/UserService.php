<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Modules\Admin\Services;

use Modules\Admin\Repositories\UserRepository;
use Illuminate\Database\DatabaseManager;
use \Illuminate\Database\Eloquent\Model;
use Modules\Admin\Models\User;
use Validator;

/**
 * Description of QuestionService
 *
 * @author mayada.fahim
 */
class UserService extends BaseService {

    public function __construct(DatabaseManager $database, UserRepository $repository) {
        $this->setDatabase($database);
        $this->setRepository($repository);
    }

    public function prepareCreate(array $data) {

        // validate the data before insertion
        $rules = User::$Rules;
        $validator = Validator::make($data, $rules);
        if ($validator->fails()) {
            return ["errors" => $validator->errors()->all()];
        }

        //encrypt password
        $data["password"] = bcrypt($data["password"]);
        $data["is_admin"] = 1;

        // create admin object
        $admin = $this->repository->create($data);

        return $admin;
    }

    public function prepareUpdate(Model $model, array $data) {
        $this->repository->update($data, $model->id);
    }

    public function prepareDelete($id) {
        $this->repository->delete($id);
    }
    
    /*
     * reset all tables in database and add one default admin
     */
    public function resetTables()
    {
        $this->repository->resetTables();
        $admin = ["username" => "admin",
            "email" => "admin@gmail.com",
            "password" => "123456",
            "is_admin" => 1];
        $this->create($admin);
    }

}
