<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Modules\Admin\Services;

use \Exception;
use Illuminate\Database\DatabaseManager;
use \Bosnadev\Repositories\Eloquent\Repository;
use \Illuminate\Database\Eloquent\Model;

/**
 * Base Service
 */
abstract class BaseService {

    /**
     *
     * @var DatabaseManager
     */
    private $database;

    /**
     *
     * @param DatabaseManager $database
     */
    public function setDatabase(DatabaseManager $database) {
        $this->database = $database;
    }

    /**
     *
     * @var Repository
     */
    protected $repository;

    /**
     *
     * @param Repository $repository
     */
    public function setRepository(Repository $repository) {
        $this->repository = $repository;
    }

    /**
     *
     * Abstract Function to be called before creating new object
     * And will be implemented by the repository Service class
     *
     */
    abstract public function prepareCreate(array $data);

    /**
     * Abstract Function to be called before Updating object
     * And will be implemented by the repository Service class
     */
    abstract public function prepareUpdate(Model $model, array $data);

    /**
     *
     * Abstract Function to be called before deleting object
     * And will be implemented by the repository Service class
     *
     */
    abstract public function prepareDelete($id);

    /**
     * Return All records of this model
     *
     * @return mixed
     */
    public function getAll() {
        return $this->repository->all();
    }

    /**
     * Return a specific model with the associate id
     *
     * @param int $id
     * @param array $columns selected columns
     * @return mixed
     */
    public function getById($id, $columns = array('*')) {
        $model = $this->repository->find($id, $columns);

        return $model;
    }

    /**
     * Create a new instance and save it to DB
     *
     * @param array $data
     * @return mixed
     * @throws Exception
     */
    public function create(array $data) {
        $this->database->beginTransaction();

        try {
            $model = $this->prepareCreate($data);
        } catch (Exception $e) {
            $this->database->rollBack();
            throw $e;
        }

        $this->database->commit();

        return $model;
    }

    /**
     * Update specific record with the associated id
     *
     * @param int $id
     * @param array $data
     * @return mixed
     * @throws Exception
     */
    public function update($id, array $data) {
        $model = $this->repository->find($id);
        $this->database->beginTransaction();

        try {
            $ret = $this->prepareUpdate($model, $data);
        } catch (Exception $e) {
            $this->database->rollBack();

            throw $e;
        }

        $this->database->commit();

        // The old object 
        return $ret;
    }

    /**
     * Delete specific record with the associated id
     *
     * @param int $id
     * @throws Exception
     */
    public function delete($id) {
        $this->database->beginTransaction();

        try {
            $this->prepareDelete($id);
        } catch (Exception $e) {
            $this->database->rollBack();

            throw $e;
        }

        $this->database->commit();
    }

}
