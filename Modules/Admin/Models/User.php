<?php

namespace Modules\Admin\Models;

use Illuminate\Database\Eloquent\Model;

class User extends Model {
    
    /*
     * Model Parameters
     */
    protected $table = 'users';
    protected $fillable = array('username', 'email', 'password', 'is_admin');

    /*
     * Validation rules before inserting or updating answer
     */
    public static $Rules = [
        'username' => 'required',
        'email' => 'required|email|unique:users',
        'password' => 'required'
    ];


}
