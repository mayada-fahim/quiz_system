<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Modules\Question\Services;

use Modules\Question\Repositories\QuestionRepository;
use Modules\Question\Repositories\AnswerRepository;
use Illuminate\Database\DatabaseManager;
use \Illuminate\Database\Eloquent\Model;
use Modules\Question\Models\Question;
use Validator;

/**
 * Description of QuestionService
 *
 * @author mayada.fahim
 */
class QuestionService extends BaseService {

    public function __construct(DatabaseManager $database, QuestionRepository $repository, AnswerRepository $answerRepository) {
        $this->setDatabase($database);
        $this->setRepository($repository);
        $this->answerRepository = $answerRepository;
    }

    public function prepareCreate(array $data) {

        // validate the data before insertion
        $rules = Question::$Rules;
        $validator = Validator::make($data, $rules);
        if ($validator->fails()) {
            return ["errors" => $validator->errors()->all()];
        }

        // create question object
        $question = $this->repository->create($data);

        // create question answers
        foreach ($data["answers"] as $answer) {
            $obj = ["answer" => $answer, "is_correct" => $data["correctAnswer"] == $answer];
            $question->answers()->create($obj);
        }
        return $question;
    }

    public function prepareUpdate(Model $model, array $data) {
        // validate the data before insertion
        $rules = Question::$Rules;
        $validator = Validator::make($data, $rules);
        if ($validator->fails()) {
            return ["errors" => $validator->errors()->all()];
        }

        // delete previous answers
        $model->answers()->delete();

        // create question answers
        foreach ($data["answers"] as $answer) {
            $obj = ["answer" => $answer, "is_correct" => $data["correctAnswer"] == $answer];
            $model->answers()->create($obj);
        }
        unset($data["_method"]);
        unset($data["_token"]);
        unset($data["answers"]);
        unset($data["code"]);
        unset($data["codeArea"]);
        unset($data["correctAnswer"]);
        $this->repository->update($data, $model->id);
    }

    public function prepareDelete($id) {
        $this->repository->delete($id);
    }

    /*
     * get question in paginatied way
     */

    public function getPaginatedList() {
        return $this->repository->getPaginatedList();
    }

    /*
     * get single question including answers
     */

    public function getWithDetails($id) {
        return $this->repository->getWithDetails($id);
    }

    /*
     * Delete all questions
     */

    public function deleteAll() {
        return $this->repository->deleteAll();
    }

    /*
     * get questions by category
     */

    public function getByCategory($categoryId, $quizId) {
        return $this->repository->getByCategory($categoryId, $quizId);
    }

    /*
     * calculate total points for submitted quiz
     */

    public function calculatePoints($data) {
        $totalPoints = 0;
        if (isset($data["answers"])) {
            for ($i = 0; $i < count($data["answers"]); $i++) {
                if (isset($data["answers"][$i])) {
                    $answer = $this->answerRepository->find($data["answers"][$i]);
                    if ($answer["is_correct"] == 1) {
                        $totalPoints += $data["questions"][$i];
                    }
                }
            }
        }
        $data["total_points"] = $totalPoints;
        return $data;
    }

}
