<?php

Route::group(['middleware' => 'web', 'prefix' => 'question', 'namespace' => 'Modules\Question\Http\Controllers'], function() {
    Route::get('/', 'QuestionController@index');
    Route::get('create/{typeId}', 'QuestionController@create');
    Route::post('store', ['as' => 'store-question', 'uses' => 'QuestionController@store']);
    Route::delete('{id}', 'QuestionController@destroy');
    Route::get("edit/{id}", 'QuestionController@show');
    Route::put("update/{id}", ['as' => 'update-question', 'uses' => 'QuestionController@update']);
    Route::delete('delete/all', ['as' => 'delete-all-question', 'uses' => 'QuestionController@deleteAll']);
});
