<?php

namespace Modules\Question\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Question\Services\CategoryService;
use Modules\Question\Services\QuestionService;
use Illuminate\Support\Facades\Redirect;

class QuestionController extends Controller {

    public function __construct(CategoryService $categoryService, QuestionService $questionService) {
        $this->middleware('auth');
        $this->categoryService = $categoryService;
        $this->questionService = $questionService;
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index() {
        $questions = $this->questionService->getPaginatedList();
        return view('question::pages.list', ["questions" => $questions]);
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create($typeId) {
        if ($typeId == "1") {
            $typeName = "True or false";
        } else if ($typeId == "2") {
            $typeName = "Multiple choice";
        }
        $categories = $this->categoryService->getAllForDropDown();
        return view('question::pages.create', ["question" => [], "typeId" => $typeId, "type" => $typeName, "categories" => $categories]);
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request) {
        $data = $request->all();
        $data = $this->prepareData($data);
        $ret = $this->questionService->create($data);
        if (isset($ret["errors"])) {
            $request->session()->flash('errors', $ret["errors"]);
            return Redirect::to('/question/create');
        } else {
            $request->session()->flash('alert-success', 'Question has been added successfully');
            return Redirect::to('/question');
        }
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show($id) {
        $question = $this->questionService->getWithDetails($id);
        if ($question["question_type_id"] == "1") {
            $typeName = "True or false";
        } else if ($question["question_type_id"] == "2") {
            $typeName = "Multiple choice";
        }

        $categories = $this->categoryService->getAllForDropDown();
        return view('question::pages.edit', ["typeId" => $question["question_type_id"], "question" => $question, "type" => $typeName, "categories" => $categories]);
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update($id, Request $request) {
        $data = $request->all();
        $data = $this->prepareData($data);
        $ret = $this->questionService->update($id, $data);
        if (isset($ret["errors"])) {
            $request->session()->flash('errors', $ret["errors"]);
            return Redirect::to('/question/edit/' . $id);
        } else {
            $request->session()->flash('alert-success', 'Question has been updated successfully');
            return Redirect::to('/question');
        }
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy($id, Request $request) {
        $question = $this->questionService->getById($id);
        if ($question) {
            $this->questionService->delete($id);
        }
        $request->session()->flash('alert-success', 'Question has been deleted successfully');
        return Redirect::to('/question');
    }
    
    /*
     * delete all questions
     */
    public function deleteAll(Request $request) {
        $this->questionService->deleteAll();
        $request->session()->flash('alert-success', 'All Questions has been deleted successfully');
        return Redirect::to('/question');
    }
    
    /*
     * prepare create and update data before sending it to service
     */
    private function prepareData($data) {
        // concat the question body and the code provided in one string
        $data["question"] = implode("*/*/", $data["questionParts"]);
        unset($data["questionParts"]);
        return $data;
    }

}
