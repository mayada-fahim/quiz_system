<?php

namespace Modules\Question\Repositories;

use Bosnadev\Repositories\Eloquent\Repository;

/**
 * Manage all category Table custom Operations
 *
 * @author mayada.fahim
 */
class CategoryRepository extends Repository {

    /**
     * Determine the model of the repository
     *
     */
    public function model() {
        return 'Modules\Question\Models\Category';
    }

}
