<?php

namespace Modules\Question\Repositories;

use Bosnadev\Repositories\Eloquent\Repository;
use DB;

/**
 * Manage all category Table custom Operations
 *
 * @author mayada.fahim
 */
class QuestionRepository extends Repository {

    /**
     * Determine the model of the repository
     *
     */
    public function model() {
        return 'Modules\Question\Models\Question';
    }
    
    /*
     * paginated list for questions
     */
    public function getPaginatedList() {
        return $this->model->selectRaw("*")
                        ->with(['questionType', 'category', 'answers' => function($query) {
                                $query->where("is_correct", 1);
                            }])
                        ->paginate(10);
    }
    
    /*
     * get specific question with its answers
     */
    public function getWithDetails($id) {
        return $this->model->selectRaw("*")->with(['answers', 'questionType'])
                        ->where("id", $id)->first();
    }
    
    /*
     * delete all questions
     */
    public function deleteAll() {
        return DB::table('question')->delete();
    }
    
    /*
     * get question by category
     */
    public function getByCategory($categoryId, $quizId) {
        return $this->model->selectRaw("*")
                        ->with(['questionType', 'category', 'answers' => function($query) {
                                $query->where("is_correct", 1);
                            }])
                        ->where("category_id", $categoryId)->paginate(10);
    }

}
