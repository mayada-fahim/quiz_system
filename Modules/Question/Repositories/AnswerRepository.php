<?php

namespace Modules\Question\Repositories;

use Bosnadev\Repositories\Eloquent\Repository;

/**
 * Manage all category Table custom Operations
 *
 * @author mayada.fahim
 */
class AnswerRepository extends Repository {

    /**
     * Determine the model of the repository
     *
     */
    public function model() {
        return 'Modules\Question\Models\Answer';
    }

}
