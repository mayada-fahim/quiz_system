<?php

namespace Modules\Question\Models;

use Illuminate\Database\Eloquent\Model;

class QuizQuestion extends Model {
    /*
     * Model Parameters
     */

    protected $table = 'quiz_questions';
    protected $fillable = array('question_id', 'quiz_id');

    /*
     * Relations
     */

    public function question() {
        return $this->belongsTo('Modules\Question\Models\Question');
    }

}
