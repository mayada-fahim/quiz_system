<?php

namespace Modules\Question\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model {
    /*
     * Model Parameters
     */

    protected $table = 'lkp_category';
    protected $fillable = array('name', 'code');

    /*
     * Relations
     */

    public function questions() {
        return $this->hasMany('Modules\Question\Models\Question');
    }

}
