<?php

namespace Modules\Question\Models;

use Illuminate\Database\Eloquent\Model;

class QuestionType extends Model {
    
    /*
     * Model Parameters
     */
    protected $table = 'lkp_questions_type';
    protected $fillable = array('type');

    /*
     * Validation rules before inserting or updating question type
     */
    public static $Rules = [
        'type' => 'required'
    ];

    /*
     * Relations
     */

    public function questions() {
       return $this->hasMany('Modules\Question\Models\Question'); 
    }

}
