<?php

namespace Modules\Question\Models;

use Illuminate\Database\Eloquent\Model;

class Question extends Model {
    /*
     * Model Parameters
     */

    protected $table = 'question';
    protected $fillable = array('question', 'question_type_id', 'category_id', 'points');

    /*
     * Validation rules before inserting or updating question
     */
    public static $Rules = [
        'question' => 'required',
        'question_type_id' => 'required|integer|exists:lkp_questions_type,id',
        'category_id' => 'required|integer|exists:lkp_category,id',
        'points' => 'required|integer'
    ];

    /*
     * Relations
     */

    public function questionType() {
        return $this->belongsTo('Modules\Question\Models\QuestionType');
    }

    public function category() {
        return $this->belongsTo('Modules\Question\Models\Category');
    }

    public function answers() {
        return $this->hasMany('Modules\Question\Models\Answer');
    }

    public function quizQuestions() {
        return $this->hasMany('Modules\Question\Models\QuizQuestion');
    }

    // cascade delete answers when question is deleted
    protected static function boot() {
        parent::boot();

        static::deleting(function($question) {
            $question->answers()->delete();
            $question->quizQuestions()->delete();
        });
    }

}
