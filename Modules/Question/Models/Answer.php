<?php

namespace Modules\Question\Models;

use Illuminate\Database\Eloquent\Model;

class Answer extends Model {
    
    /*
     * Model Parameters
     */
    protected $table = 'answers';
    protected $fillable = array('answer', 'question_id', 'is_correct');

    /*
     * Validation rules before inserting or updating answer
     */
    public static $Rules = [
        'answer' => 'required',
        'question_id' => 'required|integer|exists:questions,id',
        'is_correct' => 'required'
    ];

    /*
     * Relations
     */

    public function question() {
       return $this->belongsTo('Modules\Question\Models\Question'); 
    }

}
