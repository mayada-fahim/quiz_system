@extends('question::layouts.master')

@section('content')
<div class="container margin-top">
    <h2>List Questions</h2>

    <div class="row">
        <div class="col">
            <!--sucess message-->
            @if(Session::has('alert-success'))

            <p class="alert alert-success">{{ Session::get('alert-success') }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
            @endif

            <form action="{{ url('question/delete/all') }}" method="POST" class="delete row pull-right">
                {{ csrf_field() }}
                {{ method_field('DELETE') }}

                <button class="btn btn-danger" style="margin-bottom: 20px;">Delete All</button>
            </form>

            <table class="table table-striped table-bordered table-hover row">

                <!--Table head-->
                <thead>
                    <tr>
                        <th>Question</th>
                        <th>Category</th>
                        <th>Type</th>
                        <th>Correct Answer</th>
                        <th>Actions</th>
                    </tr>
                </thead>

                <!--Table Body-->
                <tbody>
                    @foreach ($questions as $question)
                    <tr>
                        <td>{{explode('*/*/',$question["question"])[0]}}
                            <span class="code">{{explode('*/*/',$question["question"])[1]}}</span>
                        </td>
                        <td>{{$question["category"]["name"]}}</td>
                        <td>{{$question["questionType"]["type"]}}</td>
                        <td>{{$question["answers"][0]["answer"]}}</td>
                        <td>
                            <form action="question/{{ $question->id }}" method="POST" class="delete col-md-3" style="width: 36%;">
                                {{ csrf_field() }}
                                {{ method_field('DELETE') }}

                                <button class="btn btn-danger">Delete</button>
                            </form>

                            <a href="question/edit/{{$question->id}}" class="btn btn-warning col-md-3">Edit</a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>

            <!--Pagination links-->
            <div class="col-xs-12 text-center">
                <nav aria-label="...">
                    {{ $questions->links()}}
                </nav>
            </div> 
        </div>
    </div>
</div>

@stop

<!--Required js files-->
@section('pagescript')
<script src="{{ Module::asset('Question:js/list.js') }}" /></script>
@stop