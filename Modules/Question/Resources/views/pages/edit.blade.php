@extends('question::layouts.master')

@section('content')
<div class="container margin-top">
    <h3>{{$type}}</h3>

    <!-- Form for adding a new Question-->
    {{ Form::model($question, ['route' => ['update-question', $question->id], 'method' => 'put']) }}


    <!--Question category-->
    <div class="form-group">
        {{Form::label('category', 'Please choose question category', ['class' => 'control-label']) }}
        {{Form::select('category_id', $categories,$question["category_id"], ["class" => "form-control"]) }}
    </div>

    <!-- Question Head-->
    <div class="form-group">
        {{Form::label('question', 'Please type your question', ['class' => 'control-label']) }}
        {{Form::textarea('questionParts[]',explode("*/*/",$question["question"])[0],["class" => "form-control", "required", "rows" => 3])}}
    </div>

    <!-- Question Code-->
    <div class="form-group">

        {{Form::label('code', 'If there is a programming code, enter below:', ['class' => 'control-label']) }}
        {{Form::label('code-language', 'Select language of the code: ', ['class' => 'control-label']) }}
        {{Form::select('code', ["text/x-php" => "PHP"],null, ["class" => "form-control", "id" => 'language']) }}
        {{Form::textarea('codeArea',explode("*/*/",$question["question"])[1],["class" => "form-control", "id"=>"codeArea"])}}
        {{ Form::hidden('questionParts[]', explode("*/*/",$question["question"])[1] ,["id" => "code"]) }}
    </div>

    <!--Answers-->
    <div class="form-group">

        <!--if the question is true/false-->
        @if($typeId == 1)

        {{Form::label('answer', 'Choose the answer', ['class' => 'control-label']) }}
        @foreach($question['answers'] as $answer)
        <div class="radio">
            <label>{{ Form::radio('correctAnswer', $answer["answer"], $answer["is_correct"]) }}
                {{Form::text("answers[]",$answer["answer"], ["class" => "form-control", "readonly"])}}
            </label>
        </div>
        @endforeach
        @else
        <!--multiple choice question needs 4 answers-->
        <label>
            {{Form::label('answer', 'Type the choices and choose the correct one', ['class' => 'control-label']) }}

        </label>
        @foreach($question['answers'] as $key=>$answer)
        <div class="radio">
            <label>{{ Form::radio('correctAnswer', $answer["answer"], $answer["is_correct"],['id'=>"correctAnswer".($key+1)]) }}
                {{Form::text("answers[]",$answer["answer"], ["class" => "form-control", 'id' => 'answer'.($key+1)])}}
            </label>
        </div>
        @endforeach
        @endif
    </div>

    <!--Points-->
    <div class="form-group">
        {{ Form::label('points', 'Enter points for this question', ['class' => 'control-label']) }}
        {{Form::number('points', $question["points"] , ['min' => '0' ,'class' => 'form-control','required']) }}
    </div>

    {{ Form::hidden('question_type_id', $typeId) }}
    <div class="form-group">	
        {{ Form::submit('Save', ['class' => 'btn btn-primary', 'id' => 'submit'] )}}
    </div>

    {{ Form::close() }}

    <!--Error message-->
    @if(Session::has('errors'))
    <div class="alert alert-danger">
        <ul>
            @foreach (Session::get('errors') as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
</div>

@stop

<!--Required js files-->
@section('pagescript')

<script src="{{ Module::asset('Question:js/create.js') }}" /></script>
@stop