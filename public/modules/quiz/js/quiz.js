jQuery(document).ready(function ($) {
//    warn_on_unload = "You have unsaved changes!"
    var date = new Date();
    var d2 = new Date(date);
    d2.setMinutes(date.getMinutes() + parseInt($("#time").val()));
    $('#clock').countdown(d2)
            .on('update.countdown', function (event) {
                var format = '%H:%M:%S';
                $(this).html(event.strftime(format));
            })
            .on('finish.countdown', function (event) {
                // warn_on_unload = true;
                document.getElementById('submit').submit();

            });

    // fill the table with code snippet if found
    $('.code').each(function () {

        var $this = $(this),
                $code = $this.html();

        $this.empty();

        if ($code) {
            var myCodeMirror = CodeMirror(this, {
                value: $code,
                mode: 'text/x-php',
                lineNumbers: !$this.is('.inline'),
                readOnly: true
            });
        }

    });

    var warn_on_unload = "";

    warn_on_unload = "Leaving this page will cause any unsaved data to be lost.";
    $("form").submit(function () {
        warn_on_unload = "";
    });
    window.onbeforeunload = function () {
        if (warn_on_unload != '') {
            return warn_on_unload;
        }

    }

    function disableBack() {
        window.history.forward()
    }

    window.onload = disableBack();
    window.onpageshow = function (evt) {
        if (evt.persisted)
            disableBack()
    }

});
