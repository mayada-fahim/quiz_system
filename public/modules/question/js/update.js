var myTextArea = document.getElementById("codeArea");

var myCodeMirror = CodeMirror.fromTextArea(myTextArea, {
    lineNumbers: true,
    lineWrapping: true,
    mode: $("#language").val(),
});

$("#language").on('change', function () {
    myCodeMirror.setOption("mode", $("#language").val());
});

$("#submit").on("click", function () {
    $("#code").val(myCodeMirror.getValue());
})

// updating radio buttons value with typed values
$("#answer1").on('change', function () {
    $("#correctAnswer1").val($("#answer1").val());
});

$("#answer2").on('change', function () {
    $("#correctAnswer2").val($("#answer2").val());
});

$("#answer3").on('change', function () {
    $("#correctAnswer3").val($("#answer3").val());
});

$("#answer4").on('change', function () {
    $("#correctAnswer4").val($("#answer4").val());
});