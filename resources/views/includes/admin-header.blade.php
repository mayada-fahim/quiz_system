<div class="navbar navbar-default navbar-fixed-top" role="navigation">
    <div class="container">
        <div class="navbar-header">
            <a class="navbar-brand" href="#"><i class="icon-home icon-white"> </i>{{ Auth::user()->username }}</a>
        </div>
        <div class="collapse navbar-collapse navbar-ex1-collapse">
            <ul class="nav navbar-nav"><li class="menu-item "><a href="#">Quiz HomePage</a></li>
                <li class="menu-item dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">Manage Questions <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li class="menu-item dropdown dropdown-submenu"><a href="#" class="dropdown-toggle" data-toggle="dropdown">Create Question</a>
                            <ul class="dropdown-menu">
                                <li class="menu-item "><a href="{{ url('question/create/1') }}">True/False</a></li>
                                <li class="menu-item "><a href="{{ url('question/create/2') }}">Multiple Choice</a></li>
                            </ul>
                        </li>
                        <li class="menu-item dropdown"><a href="{{ url('question') }}">All Questions</a></li>
                    </ul>

                </li>
                <li class="menu-item dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">Quiz Managment <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li class="menu-item dropdown"><a href="{{ url('quiz/create') }}">Create Quiz</a></li>
                        <li class="menu-item dropdown"><a href="{{ url('quiz') }}">All Quizzes</a></li>

                    </ul>

                </li>

                <li class="menu-item dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">Settings <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li class="menu-item dropdown"><a href="{{ url('admin/create') }}">Register new admin</a></li>
                        <li class="menu-item dropdown"><a class="reset" href="{{ url('admin/reset') }}">Reset tables</a></li>
                        <li class="menu-item dropdown"><a href="{{ url('admin/changePassword') }}">Change password</a></li>
                        <li class="menu-item dropdown">{{ Form::open(['method' => 'DELETE', 'route' => ['delete-admin'], "class" => 'delete'])}}
                            {{ Form::submit('Delete my account', ['class' => 'delete-btn']) }}
                            {{Form::close()}}</li>
                    </ul>

                </li>

            </ul>

            <ul class="nav navbar-nav navbar-right">
                <li>

                    <a href="{{ route('logout') }}"
                       onclick="event.preventDefault();
                               document.getElementById('logout-form').submit();">
                        Logout
                    </a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                    </form>

                </li>
            </ul>
        </div>
    </div>
</div> 