<meta charset="utf-8">
<meta name="description" content="">
<meta name="Quiz System" content="Scotch">
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
<title>Quiz System</title>
<link rel="alternate" href="" hreflang="en" />
<link rel="alternate" href="" hreflang="ar" />

<meta name="viewport" content="width=device-width, initial-scale=1">

<link rel="stylesheet" href="{{ URL::asset('css/bootstrap.min.css') }}" />
<link rel="stylesheet" href="{{ URL::asset('css/font-awesome.min.css') }}" />
<link rel="stylesheet" href="{{ URL::asset('css/glyphicons.css') }}" />
<link rel="stylesheet" href="{{ URL::asset('css/custom.css') }}" />
<link rel="stylesheet" href="{{ URL::asset('codemirror/lib/codemirror.css') }}" />